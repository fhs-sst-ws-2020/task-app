# Task App

Example Spring Boot backend application for managing tasks and projects.

To test the RESTful API use the requests in folder `http-client`.

## Multi-stage Docker

The `Dockerfile` contains multiple stages.

Use the following command to build, test and package this application using Docker:

```
docker build -t sst-ws2020/task-app:0.0.1-SNAPSHOT -t sst-ws2020/task-app:latest .
```
