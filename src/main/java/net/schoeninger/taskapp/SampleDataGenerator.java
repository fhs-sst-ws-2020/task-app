package net.schoeninger.taskapp;

import lombok.extern.slf4j.Slf4j;
import net.schoeninger.taskapp.domain.Project;
import net.schoeninger.taskapp.domain.Task;
import net.schoeninger.taskapp.repositories.ProjectRepository;
import net.schoeninger.taskapp.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Slf4j
@Component
public class SampleDataGenerator implements CommandLineRunner {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    @Autowired
    public SampleDataGenerator(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Generating test data..");
        List<Project> projects = generateSampleProjects(10);
        generateSampleTasks(projects, 20);
    }

    private void generateSampleTasks(List<Project> projects, int numberOfTasks) {
        Random random = new Random();
        List<Task> tasks = new ArrayList<>();
        for (Project project: projects) {
            for (int i = 1; i <= numberOfTasks; i++) {
                tasks.add(Task.builder()
                        .project(project)
                        .title("My Task #" + i)
                        .description("Lorem ipsum..")
                        .timeEstimateMinutes(10 + random.nextInt(60))
                        .actualTimeMinutes(20 + random.nextInt(60))
                        .doneAt(random.nextBoolean() ? LocalDateTime.now() : null)
                        .build());
            }
        }
        taskRepository.saveAll(tasks);
    }

    private List<Project> generateSampleProjects(int numberOfProjects) {
        List<Project> projects = new ArrayList<>();
        for (int i = 1; i <= numberOfProjects; i++) {
            projects.add(new Project(null, "My Project #" + i));
        }
        return projectRepository.saveAll(projects);
    }

}
