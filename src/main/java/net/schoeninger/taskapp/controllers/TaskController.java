package net.schoeninger.taskapp.controllers;

import net.schoeninger.taskapp.domain.Task;
import net.schoeninger.taskapp.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/projects")
public class TaskController {

    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/{projectId}/tasks")
    public List<Task> getAllTasksForProject(@PathVariable long projectId) {
        return taskService.getAllTasksForProject(projectId);
    }

    @GetMapping(value = "/{projectId}/tasks", params = "done")
    public List<Task> getFilteredTasksForProject(@PathVariable long projectId, @RequestParam boolean done) {
        return taskService.getTasksForProject(projectId, done);
    }

    @PostMapping("/{projectId}/tasks")
    @ResponseStatus(HttpStatus.CREATED)
    public Task addTaskToProject(@PathVariable long projectId, @RequestBody @Valid Task newTask) {
        return taskService.addTaskToProject(projectId, newTask);
    }

    @PutMapping("/{projectId}/tasks/{taskId}")
    public Task updateById(@PathVariable long projectId, @PathVariable long taskId, @RequestBody @Valid Task newValues) {
        return taskService.updateTaskForProjectWithId(projectId, taskId, newValues);
    }

    @DeleteMapping("/{projectId}/tasks/{taskId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long projectId, @PathVariable long taskId) {
        taskService.deleteTaskForProjectById(projectId, taskId);
    }

}
