package net.schoeninger.taskapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @Builder
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @NotBlank
    private String title;

    @Column(nullable = false)
    private String description;

    @Min(0)
    private int timeEstimateMinutes;

    @Min(0)
    private int actualTimeMinutes;

    private LocalDateTime doneAt;

    @ManyToOne // TODO: OneToMany, OneToOne,...
    @JsonIgnore
    private Project project;

}
