package net.schoeninger.taskapp.services;

import net.schoeninger.taskapp.domain.Project;

import java.util.List;

public interface ProjectService {

    List<Project> findAll();
    Project findById(long projectId);
    Project addNew(Project project);
    Project updateExisting(long projectId, Project newValues);
    void deleteById(long projectId);

}
