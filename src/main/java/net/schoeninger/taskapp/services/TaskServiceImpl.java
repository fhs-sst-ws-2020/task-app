package net.schoeninger.taskapp.services;

import net.schoeninger.taskapp.domain.Project;
import net.schoeninger.taskapp.domain.Task;
import net.schoeninger.taskapp.repositories.TaskRepository;
import net.schoeninger.taskapp.util.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;
    private ProjectService projectService;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository, ProjectService projectService) {
        this.taskRepository = taskRepository;
        this.projectService = projectService;
    }

    @Override
    public List<Task> getAllTasksForProject(long projectId) {
        Project project = projectService.findById(projectId);
        return taskRepository.findAllByProject(project);
    }

    @Override
    public List<Task> getTasksForProject(long projectId, boolean filterDone) {
        Project project = projectService.findById(projectId);
        if (filterDone) {
            return taskRepository.findAllDoneTasksByProject(project);
        } else {
            return taskRepository.findAllOpenTaskyByProject(project);
        }
    }

    @Override
    public Task addTaskToProject(long projectId, Task newTask) {
        Project project = projectService.findById(projectId);
        newTask.setId(null);
        newTask.setProject(project);
        return taskRepository.save(newTask);
    }

    @Override
    public Task updateTaskForProjectWithId(long projectId, long taskId, Task newValues) {
        Project project = projectService.findById(projectId);
        Task task = getTaskByIdAndProject(taskId, project);
        task.setTitle(newValues.getTitle());
        task.setDescription(newValues.getDescription());
        task.setTimeEstimateMinutes(newValues.getTimeEstimateMinutes());
        task.setActualTimeMinutes(newValues.getActualTimeMinutes());
        task.setDoneAt(newValues.getDoneAt());
        return taskRepository.save(task);
    }

    @Override
    public void deleteTaskForProjectById(long projectId, long taskId) {
        Project project = projectService.findById(projectId);
        Task task = getTaskByIdAndProject(taskId, project);
        taskRepository.delete(task);
    }

    private Task getTaskByIdAndProject(long taskId, Project project) {
        return taskRepository.findByProjectAndId(project, taskId)
                .orElseThrow(() -> new EntityNotFoundException("Couldn't find task for projectId " + project.getId() + " and taskId " + taskId));
    }

}
